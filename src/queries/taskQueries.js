import {gql} from 'apollo-boost';

const GET_ALL_TASKS = gql`
		{
			tasks{
				id
				name
				
			}
		}
`

const ADD_TASK = gql `
	mutation
		addTask(
			$title: String!,
			$body: String!,
			$status: String!,
			$categoryId: ID!,
			$projectId: ID!
		){
			addTask(
				title: $title,
				body: $body,
				status: $status,
				categoryId: $categoryId,
				projectId: $projectId
			){
				id
				title
				body
				status
				categoryId
				projectId
				category{
					name
				}
				project{
					id
					name
				}
			}
		}
`

const UPDATE_TASK = gql `
	mutation
		updateTask(
			$id: ID!,
			$name: String!
		){
			updateTask(
				id: $id,
				name: $name
			){
				id
				name
			}
		}
`


const DELETE_TASK = gql `
	mutation deleteTask($id:ID!){
		deleteTask(id:$id){
			id
		}
	}
`

export {
	GET_ALL_TASKS,
	ADD_TASK,
	UPDATE_TASK,
	DELETE_TASK
}
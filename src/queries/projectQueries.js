import {gql} from 'apollo-boost';

const GET_ALL_PROJECTS = gql`
		{
			projects{
				id
				name
				description
				status
				tasks{
					id
					title
					body
					status
					category{
						id
						name
					}
				}
			}
		}
`

const ADD_PROJECT = gql `
	mutation
		addProject(
			$name: String!, 
			$description: String!,
			$status: String
		){
			addProject(
				name: $name,
				description: $description,
				status: $status
			){
				id
				name
			}
		}
`

const UPDATE_PROJECT = gql `
	mutation
		updateProject(
			$id: ID!,
			$name: String!, 
			$description: String
		){
			updateProject(
				id: $id,
				name: $name,
				description: $description
			){
				id
				name
				description
			}
		}
`


const DELETE_PROJECT = gql `
	mutation deleteProject($id:ID!){
		deleteProject(id:$id){
			id
		}
	}
`

export {
	GET_ALL_PROJECTS,
	ADD_PROJECT,
	UPDATE_PROJECT,
	DELETE_PROJECT
}
import {gql} from 'apollo-boost';

const GET_ALL_CATEGORIES = gql`
		{
			categories{
				id
				name
				
			}
		}
`

const ADD_CATEGORY = gql `
	mutation
		addCategory(
			$name: String!
		){
			addCategory(
				name: $name
			){
				id
				name
			}
		}
`

const UPDATE_CATEGORY = gql `
	mutation
		updateCategory(
			$id: ID!,
			$name: String!
		){
			updateCategory(
				id: $id,
				name: $name
			){
				id
				name
			}
		}
`


const DELETE_CATEGORY = gql `
	mutation deleteCategory($id:ID!){
		deleteCategory(id:$id){
			id
		}
	}
`

export {
	GET_ALL_CATEGORIES,
	ADD_CATEGORY,
	UPDATE_CATEGORY,
	DELETE_CATEGORY
}
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import "bootstrap/dist/css/bootstrap.css";

const client = new ApolloClient({
  uri: "http://localhost:4000/graphql",
  onError: ({graphQLErrors, networkError})=>{
  	if(graphQLErrors){
  		graphQLErrors.forEach(({
  			message, location, path
  		})=>{
  			console.log(`[GraphQL Error]: Message: ${message}, Location: ${location}, Path: ${path}`)
  		})
  	}
  }
});


toast.configure({
  autoClose: 4000,
  draggable: false,
  hideProgressBar: true,
  newestOnTop: true,
  closeOnClick: true
})

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

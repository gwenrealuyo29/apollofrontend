import React, {useState} from 'react';
import {graphql} from 'react-apollo';
import {GET_ALL_PROJECTS, ADD_PROJECT, UPDATE_PROJECT, DELETE_PROJECT} from './queries/projectQueries';
import {GET_ALL_CATEGORIES} from './queries/categoryQueries';
import {GET_ALL_TASKS, ADD_TASK, UPDATE_TASK, DELETE_TASK} from './queries/taskQueries';
import {Table, Col, Button} from 'reactstrap';
import {Projects, ProjectForm, Tasks} from './components';
import {compose} from 'recompose';
import {AddToast, UpdateToast, DeleteToast} from './globalcomponents/Toasts'


const App = (props) => {

  const [showForm, setShowForm] = useState(false);
  const [showTasks, setShowTasks] = useState(false);
  const [showAddTask, setShowAddTask] = useState(false);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [edit, setEdit] = useState(false);
  const [id, setId] = useState('');
  const [tasks, setTasks] = useState([]);
  const [taskName, setTaskName] = useState('');
  const [taskBody, setTaskBody] = useState('');
  const [taskCategory, setTaskCategory] = useState('');
  const [projectId, setProjectId] = useState('');
  // const [categoryId, setCategoryId] = useState('');



  const handleShowForm = () => {
    setShowForm(!showForm);
    setEdit(false);
  }

  const handleNameChange = e => {
    setName(e.target.value);
  }

  const handleDescriptionChange = e => {
    setDescription(e.target.value);
  } 

  const handleAddTask = e => {
    setTaskName(e.target.value);
  }

  const handleTaskBody = e => {
    setTaskBody(e.target.value);
  }

  const handleSaveProject = e => {

    if(edit){
      props.updateProject({
        variables: {id, name, description}
      }).then(res=>{
        setShowForm(false)
        UpdateToast(name);
      })
    } else {
      props.addProject({
        variables: {
          name,
          description,
          status: "Pending"
        },
        refetchQueries: [{query: GET_ALL_PROJECTS}]
      }).then(res=>{
        setShowForm(false);
        AddToast(name, 'Projects');
      })
    }

    
  }

  const handleEdit = project => {
    setEdit(true);
    setName(project.name);
    setDescription(project.description);
    setShowForm(true);
    setId(project.id);


  }

  const handleDelete = id => {
    props.deleteProject({
      variables: {id},
      refetchQueries: [{query: GET_ALL_PROJECTS}]
    }).then(res=>{
      DeleteToast('Yey')
    })
  }

  const handleShowTasks = project => {
    setShowTasks(!showTasks);

    if(!showTasks){
      setTasks(project.tasks);
      setProjectId(project.id);
    }
  }

  const handleShowAddTask = project => {
    setShowAddTask(!showAddTask);
  }

  const handleSaveTask = () => {
    props.addTask({
      variables: {
        title: taskName,
        body: taskBody,
        status: "Pending",
        categoryId: taskCategory,
        projectId: projectId
      },
      refetchQueries: [{query: GET_ALL_PROJECTS}]
    }).then(res=>{
      console.log(res);
      let taskArray = [...tasks];
      taskArray.push(res.data.addTask);
      setTasks(taskArray)
      AddToast(res.data.addTask.title, "Tasks");
      setShowAddTask(!showAddTask)
    })
  }

  const handleTaskCategory = id => {
    setTaskCategory(id.id);
  }





  
  if(props.projects.loading) return <h1>Loading...</h1>
  if(props.projects.error) return <h1>Failed to load. Sorry.</h1>



  console.log(props)
  console.log(props.categories.categories)
  console.log(taskCategory)
  console.log(props.addTask)
  

  return(
    <React.Fragment>
      <Tasks
        showTasks={showTasks}
        handleShowTasks={handleShowTasks}
        tasks={tasks}
        handleShowAddTask={handleShowAddTask}
        showAddTask={showAddTask}
        handleAddTask={handleAddTask}
        handleSaveTask={handleSaveTask}
        categories={props.categories.categories}
        handleTaskCategory={handleTaskCategory}
        handleTaskBody={handleTaskBody}
        categories={props.categories.categories}

      />

      <Col lg={12}>
          <h1 className="text-center py-5">My Projects</h1>
          <Button color="info" onClick={handleShowForm}>+ Project</Button>
          <ProjectForm
              handleShowForm={handleShowForm}
              showForm={showForm}
              handleNameChange={handleNameChange}
              handleDescriptionChange={handleDescriptionChange}
              name={name}
              description={description}
              handleSaveProject={handleSaveProject}
              edit={edit}
          />
          <p></p>
      
          <Table>
            <thead> 
              <tr>
                <th>Project Name</th>
                <th>Project Description</th>
                <th>Status</th>
                <th>Tasks</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <Projects
                projects = {props.projects.projects}
                handleEdit={handleEdit}
                handleDelete={handleDelete}
                handleShowTasks={handleShowTasks}
              />

            </tbody>
          </Table>
       
      </Col>

      
    </React.Fragment>
  )
}

export default compose(
    graphql(GET_ALL_PROJECTS, {name: "projects"}),
    graphql(ADD_PROJECT, {name: "addProject"}),
    graphql(UPDATE_PROJECT, {name: 'updateProject'}),
    graphql(DELETE_PROJECT, {name: "deleteProject"}),
    graphql(GET_ALL_CATEGORIES, {name: "categories"}),
    graphql(GET_ALL_TASKS, {name: "tasks"}),
    graphql(ADD_TASK, {name: "addTask"}),
    graphql(UPDATE_TASK, {name: 'updateTask'}),
    graphql(DELETE_TASK, {name: "deleteTask"})
  )
(App);
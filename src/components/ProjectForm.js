import React from 'react';
import {Modal, ModalHeader, ModalBody, Button} from 'reactstrap';
import {FormInput} from '../globalcomponents'

const ProjectForm = props => {
	return(
		<Modal
			isOpen={props.showForm}
			toggle={props.handleShowForm}
		>
			<ModalHeader toggle={props.handleShowForm} className="bg-info">
				<span className='text-light'>{props.edit ? 'Edit Project' : 'Add Project'}</span>
			</ModalHeader>
			<ModalBody>
				<FormInput
					label={'Project Name'}
					placeholder={'Enter project name'}
					type={'text'}
					onChange={props.handleNameChange}
					defaultValue={props.edit ? props.name : ''}
				/>
				<FormInput
					label={'Project Description'}
					placeholder={'Enter project description'}
					type={'text'}
					onChange={props.handleDescriptionChange}
					defaultValue={props.edit ? props.description : ''}
				/>
				<Button 
					color="success"
					disabled={props.name == "" || props.description == "" ? true : false}
					onClick={props.handleSaveProject}
				>{props.edit ? 'Update' : 'Add'}</Button>
			</ModalBody>
		</Modal>
	)
}

export default ProjectForm;
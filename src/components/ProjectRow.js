import React from 'react';
import {Button} from 'reactstrap';

const ProjectRow = props => {
	return(
		<React.Fragment>
			<tr>
				<td>{props.project.name}</td>
				<td>{props.project.description}</td>
				<td>{props.project.status}</td>
				<td>{props.project.tasks.length}</td>
				<td>
					<Button
						onClick={()=>props.handleEdit(props.project)}
					>Edit Project</Button>
					<Button
						color="danger"
						onClick={()=>props.handleDelete(props.project.id)}
					>
						Delete Project
					</Button>
					<Button
						color="info"
						onClick={()=>props.handleShowTasks(props.project)}
					>
						View Tasks
					</Button>
				</td>
			</tr>
		</React.Fragment>
	)
}

export default ProjectRow;
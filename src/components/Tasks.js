import React, {useState} from 'react';
import {Modal, ModalHeader, ModalBody, Button, Input, InputGroup, Dropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap';
import {FormInput} from '../globalcomponents'

const Tasks = props => {

	const [dropdownOpen, setDropdownOpen] = useState(false);
	const toggle = () => setDropdownOpen(!dropdownOpen);

	return(
		<Modal
			isOpen={props.showTasks}
			toggle={props.handleShowTasks}
		>
			<ModalHeader
				toggle={props.handleShowTasks}
			>Tasks
			</ModalHeader>
			<ModalBody>
				<div className="d-flex justify-content-end">
					<Button
						color="info"
						onClick={props.handleShowAddTask}
					> + Task 
					</Button> 
				</div>
				{props.showAddTask ? 
					<React.Fragment>
							<FormInput 
								placeholder={"Enter Task Title"}
								type={"text"}
								onChange={props.handleAddTask}
							/>
							<FormInput 
								placeholder={"Enter Task Body"}
								type={"text"}
								onChange={props.handleTaskBody}
							/>
							<Dropdown
								isOpen={dropdownOpen}
								toggle={toggle}
							>
								<DropdownToggle caret>
									Choose Category
								</DropdownToggle>
								<DropdownMenu>
									{props.categories.map(category=>(
											<DropdownItem
												key={category.id}
												onClick={()=>props.handleTaskCategory(category)}
											>{category.name}
											</DropdownItem>
										))}
								</DropdownMenu>
							</Dropdown>
							<p></p>
							<Button
								block
								color="success"
								onClick={props.handleSaveTask}
							>Submit
							</Button>
					</React.Fragment>
					 : ""}

				{props.tasks.map(task=>(
					<InputGroup
						key={task.id}
					>
						<Input 
							value={task.title}
						/>
					</InputGroup>
					))}
			</ModalBody>
		</Modal>
	)
}

export default Tasks;
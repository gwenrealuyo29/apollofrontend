import React from 'react';
import {ProjectRow} from '../components'

const Projects = props => {
	return(
		<React.Fragment>
			{props.projects.map(project=>(
				<ProjectRow
					key={project.id}
					project={project}
					handleEdit={props.handleEdit}
					handleDelete={props.handleDelete}
					handleShowTasks={props.handleShowTasks}
				/>
			))}
		</React.Fragment>
	)
}

export default Projects;
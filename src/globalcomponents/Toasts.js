import React from 'react';
import {toast} from 'react-toastify';

const DeleteToast = name => {
	toast.error(`Successfully deleted ${name}`);
}

const AddToast = (name, model) => {
	toast.success(`${name} added successfully to ${model}`);
}

const UpdateToast = (name) => {
	toast.success(`Updated ${name} successfully!`);
}

const ErrorToast = message => {
	toast.error(`${message}`);
}

export {
	DeleteToast,
	AddToast,
	UpdateToast,
	ErrorToast
}
import React from "react";
import { FormGroup, Label, Input } from "reactstrap";

const FormInput = ({
  label,
  name,
  type,
  placeholder,
  onChange,
  defaultValue,
  onBlur,
  required,
  autoFocus,
  ...props
}) => {
  return (
    <React.Fragment>
      <FormGroup>
        <Label>{label}</Label>
        <Input
          name={name}
          type={type}
          placeholder={placeholder}
          defaultValue={defaultValue}
          onChange={onChange}
          onBlur={onBlur}
          autoFocus={autoFocus}
          style={required ? { border: "sold 1px red" } : null}
        />
      </FormGroup>
    </React.Fragment>
  );
};

export default FormInput;